class CategoryModel {

  constructor(inject) {
    this.http = inject.http;
    this.logger = inject.logger;
  }

  processData(category) {
    let data = {
      id : category['id'],
      name : category['name'],
      // name_en : category['name'],
      slug : category['id'].toString(),
      parent_id : category['parent'].toString()
    }
    data['digital'] = '0';
    return data;
  }
  
  set(categoryData) {
    return new Promise((resolve, reject) => {
      this.http('/cats', this.processData(categoryData))
        .then(res => {
          console.log('err s', res.data);
          resolve(res);
        })
        .catch(err => {
          this.logger(['set category http', err])
        })
    });
  }

}

module.exports = CategoryModel;