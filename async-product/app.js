const { apiUrl } = require('./global');
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;
const axios = require('axios');
const CategoryModel = require('./libs/category-model');
const ProductModel = require('./libs/product-model');
const ImageLib = require('./libs/product-model');
const $CategoryModel = '';
const $ImageLib = '';
const fs = require('fs');

const WooApi = new WooCommerceRestApi({
  url: 'https://adv-line.com/',
  consumerKey: 'ck_1fa70cc1de643bfda956270335430c7d24c39ab2',
  consumerSecret: 'cs_b83bb1065645cf0487fe06d56ad65232675010d1',
  wpAPI: true,
  version: 'wc/v3'
});

const http = function (url, params = {}) {
  const options = {
    headers : {
      'Content-Type' : 'application/json'
    }
  };
  return axios.post(apiUrl + url, params, options);
}

const logger = function (err, category = null) {
  // console.log(err);
}

async function setCategories() {
  let categoryModel = new CategoryModel({http, logger});

  // let categories = await loadCategories();
  let categories = JSON.parse(fs.readFileSync('./db/categories.json', 'utf-8'));

  for(let i in categories ) {
    let category = categories[i];
    // console.log('cat', category);
    // break;
    let data = await categoryModel.set(category)
    .catch(err => {
      // logger(['category error ', category['id'], err]);
      console.log('err', err);
      return null;
    });

    console.log('data', category['name']);
    // break;
  } 
}

async function setProduct(id) {
  let productModel = new ProductModel({http, logger});
  let product = await WooApi.get('products/' + id).then(res => {
    return res.data;
  })
  .catch(err => {
    console.log('error page', err);
  });
  
  let p = await productModel.set(product).catch(err => {
    console.log('err', err);
  });

  console.log('p', p)
}

async function setProducts() {

  let productModel = new ProductModel({http, logger});

  for(let pageNumber = 1; pageNumber <= 20; pageNumber++) {

    let products = await WooApi.get('products', {
      page : pageNumber,
      per_page : 100
    }).then(res => {
      if (Array.isArray(res.data)) {
        console.log('products');
        return res.data;
      }
      console.log('this page is empty', pageNumber);
      return [];
    })
    .catch(err => {
      console.log('error page', pageNumber, err);
      return [];
    });

    if (products.length > 0) {
      for(let i in products) {
        let product = products[i];
        console.log('getting', product['id']);
        let p = await productModel.set(product).catch(err => {
            console.log('err', err);
          });
        // break;
      }
    }

    // break;
  }

}

async function insertProductsByCategory() {
  let addedProducts = [];
  let productModel = new ProductModel({http, logger});
  let categories = JSON.parse(fs.readFileSync('./db/categories.json', 'utf-8'));

  for(let i in categories ) {
    let category = categories[i];
    console.log('getting category', category['id'], category['name'])
    let products = await WooApi.get('products', {
      category : category['id'].toString(),
      per_page : 100
    }).then(res => {
      if (Array.isArray(res.data)) {
        console.log('products');
        return res.data;
      }
      console.log('this page is empty', pageNumber);
      return [];
    })
    .catch(err => {
      console.log('error page', pageNumber, err);
      return [];
    });

    if (products.length > 0) {
      for(let i in products) {
        let product = products[i];
        if (addedProducts.includes(product['id'])) {
          console.log('added product', product['id']);  
          continue;
        }

        console.log('getting', product['id']);
        let p = await productModel.set(product)
          .then(res => {
            addedProducts.push(product['id'])
          })
          .catch(err => {
            console.log('err', err);
          });
        // break;
      }
    }

  }

}

// setProduct('13116');
insertProductsByCategory();
// setProducts();
// setCategories();