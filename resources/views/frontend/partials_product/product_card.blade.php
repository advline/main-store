<div class="product-card-box product-card-box-default aiz-card-box border border-light rounded hov-shadow-md my-2 has-transition">
  <div class="position-relative">
      <a href="#" class="d-block">
          <img
              class="img-fit lazyload mx-auto h-140px h-md-210px"
              src="{{ static_asset('assets/img/placeholder.jpg') }}"
              data-src="{{ $product['image'] }}"
              onerror="this.onerror=null;this.src='{{ static_asset('assets/img/placeholder.jpg') }}';"
          >
      </a>
      <div class="absolute-top-right aiz-p-hov-icon">
          <a href="javascript:void(0)"  data-toggle="tooltip" data-title="{{ translate('Add to wishlist') }}" data-placement="left">
              <i class="la la-heart-o"></i>
          </a>
          <a href="javascript:void(0)"  data-toggle="tooltip" data-title="{{ translate('Add to compare') }}" data-placement="left">
              <i class="las la-sync"></i>
          </a>
          <a href="javascript:void(0)"  data-toggle="tooltip" data-title="{{ translate('Add to cart') }}" data-placement="left">
              <i class="las la-shopping-cart"></i>
          </a>
      </div>
  </div>
  <div class="p-md-3 p-2 text-left">
    <h3 class="product-title fw-600 fs-13 text-truncate-2 lh-1-4 mb-0 h-35px">
      <a href="#" class="d-block text-reset">{{  $product['title']  }}</a>
    </h3>
      <div class="fs-15">
          <span class="fw-700 text-primary">{{ $product['price'] }}</span>
      </div>
      <div class="rating rating-sm mt-1">
          {{ 4 }}
      </div>

  </div>
</div>