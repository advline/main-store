const ImageLib = require('./image-lib');

class ProductModel {

  constructor(inject) {
    this.http = inject.http;
    this.logger = inject.logger;
    this.productData = null;
  }


  saveAttributes() {
    // this
  }

  processData(data) {
    let category_id = 0;
    if (Array.isArray(data['categories'])) {
      if (data['categories'].length != 0) {
        if ('id' in data['categories'][0]) {
          category_id = data['categories'][0]['id'];
        }
      } 
    }

    let publish = 1;
    if (data['status'] != 'publish') {
      publish = 0;
    }

    const new_data = {
      id : data['id'],
      name : data['name'],
      description : data['short_description'] + ' <br><br> ' + data['description'],
      unit: 'قطعة',
  
      'tags' : JSON.stringify([{value : 'general'}]),
  
      "video_provider": "youtube",
      "video_link": "",
      
      published : publish,

      category_id : category_id,
      thumbnail_img : data['thumbnail'],
      photos : data['photos'].join(','),
      unit_price : data['price'],
      purchase_price : data['price'],
      current_stock : data['stock_quantity'],
    }

    return new_data;
  }

  async set(productData) {
    return new Promise(async (resolve, reject) => {
      delete productData['yoast_head'];
      this.productData = productData;
      
      productData['thumbnail'] = await ImageLib.getThumbnailImage(productData['images']);
      productData['photos'] = await ImageLib.uploadMultiImages(productData['images']);

      // console.log('data', this.processData(productData))
      this.http('/create', this.processData(productData))
        .then(res => {
          console.log('product insert');
          resolve(res);
        })
        .catch(err => {
          this.logger('http err', err)
        })
    });
  }

}

module.exports = ProductModel;