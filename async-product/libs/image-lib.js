const { apiUrl, images_dir_path, uploadUrl } = require('./../global.js');
const fs = require('fs');
const request = require('request');
const http = require('http');

class ImageLib {

  getImageUrl(image) {
    return image['src'];
  }

  async uploadMultiImages(urls) {
    // console.log('with', url)
    let ids = [];
    
    for(let i = 0; i < urls.length; i++) {
      let id = await this.uploadImage(this.getImageUrl(urls[i]));
      ids.push(id);
    }
  
    return ids;
  }
  
  async getThumbnailImage(urls) {
    // https://adv-line.com/wp-content/uploads/2020/10/Dell-OptiPlex5-300x300.jpg
    // https://adv-line.com/wp-content/uploads/2020/10/Dell-OptiPlex5.jpg
    try {
      let imageUrl = this.getImageUrl(urls[0]);
      let firstImage;
      if (imageUrl.includes('-600x600.jpg')) {
        firstImage = imageUrl.replace('-600x600.jpg', '-300x300.jpg');
      } else {
        firstImage = imageUrl.replace('.jpg', '-300x300.jpg');
      }
      
      // let fileName = firstImage.split('/')[firstImage.split('/').length - 1].replace('.jpg', '-300x300.jpg');
      
      return await this.uploadImage(firstImage);
    } catch(err) {
      return err;
    }
  }
  
  async downloadImage(imageUrl) {
    return new Promise((resolve, reject) => {
      imageUrl = imageUrl.replace('https://', 'http://');
      let splitUrl = imageUrl.split('/'); //'http://adv-line.com/wp-content/uploads/2020/11/Product-style566-1.jpg';
      let file_name = splitUrl[splitUrl.length - 1];
      const file = fs.createWriteStream("./images/" + file_name);
      const request = http.get(imageUrl, function(response) {
        response.pipe(file);
        file.on('finish', function() {
          file.close();
          resolve(file_name)
        });
  
        // check for request error too
        request.on('error', (err) => {
          return reject(err);
        });
  
        file.on('error', (err) => { // Handle errors
          return reject(err);
        });
      });
    })
  }
  
  async uploadImage(imageUrl) {
    return new Promise(async (resolve, reject) => {
      try {
        let local_file_name = await this.downloadImage(imageUrl);
  
        let uploading_url = apiUrl + '/upload';
        var req = request.post(uploading_url, function (err, resp, body) {
          if (err) {
            console.log('Error!');
          } else {
            resolve(body);
          }
        });
  
        var form = req.form();
        form.append('aiz_file', fs.createReadStream(images_dir_path + local_file_name));
      }
      catch(err) {
        console.log('upload image error', err);
        reject(err);
      }
    });
  }

}

module.exports = new ImageLib();